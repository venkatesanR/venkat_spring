package com.springdev.cfc;

public class GenericFactory {
	private static SpringCoreHolder clientService = new SpringCoreHolder();

	private GenericFactory() {
	}

	public SpringCoreHolder createClientServiceInstance() {
		return clientService;
	}
}

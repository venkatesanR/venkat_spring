package com.springdev.cfc;

public class GenericBean {

	private String beanInfo;
	private NestedBean genericNestBean;

	public String getBeanInfo() {
		return beanInfo;
	}

	public void setBeanInfo(String beanInfo) {
		this.beanInfo = beanInfo;
	}

	public NestedBean getGenericNestBean() {
		return genericNestBean;
	}

	public void setGenericNestBean(NestedBean genericNestBean) {
		this.genericNestBean = genericNestBean;
	}

	public static class NestedBean {
		public NestedBean() {
			System.out.println("Generice Nested Bean Initialized");
		}
	}
}

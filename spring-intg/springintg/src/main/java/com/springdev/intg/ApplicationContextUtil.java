package com.springdev.intg;


public class ApplicationContextUtil extends ApplicationContextLoader {

	public ApplicationContextUtil() {
		super();
	}

	@SuppressWarnings("unchecked")
	public <T> T getBean(String beanKey) {
		return (T) appContextRef.getBean(beanKey);
	}

}

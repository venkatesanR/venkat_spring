package com.springdev.intg;

import com.springdev.cfc.GenericBean;

public class SpringHelper {
	public static void main(String args[]) {
		ApplicationContextUtil springutils=new ApplicationContextUtil();
		GenericBean genericBean=springutils.getBean("genericBean");
		System.out.println(genericBean.getBeanInfo());
	}
}
